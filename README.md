Spacejunk Unity Project

Source code repository for Unity project Spacejunk_v0.

### Setup ###
* Launch Unity 5
* Open existing project folder at root level (Spacejunk_Repo1)
* Hit 'Play' to start game with current codebase

### Helpful Links ###
* [Spacejunk DevBlog OneNote](https://onedrive.live.com/edit.aspx/Documents/OneNote/Game%20Dev/Spacejunk%20DevBlog)
* [GameDev OneNote](https://onedrive.live.com/edit.aspx/Documents/OneNote/Game%20Dev/GameDev)
* [GameDev Tutorials OneNote](https://onedrive.live.com/edit.aspx/Documents/OneNote/Game%20Dev/GameDev%20Tutorials)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contributors ###

* andoink