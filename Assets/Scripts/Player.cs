﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {

	public int attackDamage = 1;
	public AudioClip[] moveSounds;
	public AudioClip oxygenPickupSound;

	protected int oxygen;

	private Animator animator;
	private Vector2 touchOrigin = -Vector2.one;		// Touchscreen support
    private int previousHorizontal = 0;
    private int previousVertical = 0;

    public int Oxygen
	{
		get{ return oxygen; }
		set{ oxygen = value; if (oxygen < 0) oxygen = 0;}
	}

	// Overriden Start from MovingObject base class
	protected override void Start () 
	{
		animator = GetComponent<Animator> ();

		// Alternative: Set Oxygen in GameManager.OnSceneLoaded
		//Oxygen = GameManager.instance.initialPlayerOxygen;

		base.Start ();
	}

	// Set current oxygen on GameManager when player disabled (level change)
	private void OnDisable()
	{
        // TODO: Save player oxygen into PlayerManager for loading next level?

		//GameManager.instance.initialPlayerOxygen = Oxygen;
	}

	// Update is called once per frame
	void Update()
	{
		// If it is not the player's turn, return
		if (!GameManager.instance.playersTurn 
			|| base.IsObjectMoving() 
			|| oxygen <= 0)
			return;

		int horizontal = 0;
		int vertical = 0;

		//#if UNITY_STANDALONE

		horizontal = (int)Input.GetAxisRaw("Horizontal");
		vertical = (int)Input.GetAxisRaw("Vertical");

		// If moving horizontally, set vertical to 0 (no diagonal movement)
		if (horizontal != 0)
			vertical = 0;

//#else

		// Check for any touch input
		if (Input.touchCount > 0)
		{
			// Get the first touch input
			Touch myTouch = Input.touches[0];

			// Set the touch origin at the beginning of the touch
			if (myTouch.phase == TouchPhase.Began)
			{
				touchOrigin = myTouch.position;
			}
			
			// Set the touch end at the end of the touch, if within the screen
			else if (myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0)
			{
				Vector2 touchEnd = myTouch.position;

				float touchDirectionX = touchEnd.x - touchOrigin.x;
				float touchDirectionY = touchEnd.y - touchOrigin.y;
				
				// Reset touch origin to ensure conditional stops evaluating true
				touchOrigin.x = -1;

				// Determine general direction
				//		If X > Y, swiping right or left?
				// 		If Y > X, swiping up or down?
				if (Mathf.Abs(touchDirectionX) > Mathf.Abs(touchDirectionY))
					horizontal = touchDirectionX > 0 ? 1 : -1;
				else
					vertical = touchDirectionY > 0 ? 1 : -1;
			}
		}

//#endif

        // If there is any input, call AttemptMove and check for Walls
        if (horizontal != 0 || vertical != 0)
		{
            // If continuous movement was previously stopped, see if direction has changed
            if (base.IsContinuousMovementStopped() &&
                !(horizontal == previousHorizontal && vertical == previousVertical))
            {
                base.ResetContinuousMovement();
            }

            // Do not attempt to move if flag has been set to stop continuous input
            // (set to true whenever movement was restricted)
            if (!base.IsContinuousMovementStopped())
            {
                bool playerMoved = false;
                playerMoved = AttemptMove<Wall>(horizontal, vertical);
            }
		}
        else
        {
            // Lift the flag for preventing continuous movement input
            if (base.IsContinuousMovementStopped())
            {
                base.ResetContinuousMovement();
            }
        }

        previousHorizontal = horizontal;
        previousVertical = vertical;
    }

	// Overridden AttemptMove implementation
	protected override bool AttemptMove<T>(int xDirection, int yDirection)
	{
		// Decrement oxygen when attempting to move
		Oxygen--;
		GameManager.instance.UpdateOxygenUI(-1);

		bool playerMoved = false;
		playerMoved = base.AttemptMove<T> (xDirection, yDirection);
        
		if (playerMoved)
		{
			bool randomizeMoveSoundPitch = true;
			AudioManager.instance.RandomizeSoundEfx(randomizeMoveSoundPitch, moveSounds);
		}

		GameManager.instance.playersTurn = false;

		return playerMoved;
	}

	// Test collisions with other BoxCollider2Ds for triggers
	private void OnTriggerEnter2D(Collider2D other)
	{
		// If colliding with Exit, call Restart with a delay and disable player
		if (other.tag == "Exit")
		{
			Invoke ("Restart", GameManager.instance.levelEndDelay);
			enabled = false;
		}

		// If colliding with Oxygen, add to oxygen value and remove it from the board
		else if (other.tag == "Oxygen")
		{
			Oxygen += GameManager.instance.pointsPerOxygenTank;
			GameManager.instance.UpdateOxygenUI(GameManager.instance.pointsPerOxygenTank);

			AudioManager.instance.PlaySingle(oxygenPickupSound);

			other.gameObject.SetActive (false);
		}
	}

	// Overridden implementation of OnCantMove
	protected override void OnCantMove<T>(T component)
	{
        // For players, check that input component is a Wall
        Wall hitWall = component as Wall;

		// Apply damage to wall and animate player
		hitWall.DamageWall (attackDamage);

		//animator.SetTrigger("playerAttack");
	}

	// Reload the level
	private void Restart()
	{
        GameManager.instance.LoadNextLevel();
	}

	// Remove oxygen from the player when hit by an enemy
	public void LoseHealth(int loss)
	{
		// Trigger the PlayerHurt animation
		animator.SetTrigger ("playerHit");

		// Remove oxygen points and check if the game is over
		Oxygen -= loss;
		GameManager.instance.UpdateOxygenUI(-loss);
		GameManager.instance.CheckGameOver();
	}
}
