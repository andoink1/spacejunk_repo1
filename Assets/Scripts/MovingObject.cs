﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour {

	public float moveTime = 0.1f;
	public LayerMask blockingLayer;

	private BoxCollider2D boxCollider;
	private Rigidbody2D rigidBody;
	private float inverseMoveTime;
	private bool objectMoving = false;
    private bool stopContinuousMovement = false;

	// Use this for initialization
	protected virtual void Start () 
	{
		boxCollider = GetComponent<BoxCollider2D> ();
		rigidBody = GetComponent<Rigidbody2D> ();

		// Reciprocal moveTime for efficient multiplication later
		inverseMoveTime = 1f / moveTime;
	}

	protected Vector2 GetEndPositionFromStartAndDirections(Vector2 startPosition, int xDirection, int yDirection)
	{
		Vector2 endPosition = startPosition + new Vector2 (xDirection, yDirection);

		return endPosition;
	}

	// Determine if an object can move into the designated position
	//protected bool CanMove(Vector2 endPosition, out RaycastHit2D hit)
	protected bool CanMove(int xDirection, int yDirection, out Vector2 endPosition, out RaycastHit2D hit)
	{
        bool canMove = false;

		// Translate current Vector3 position to Vector2 (excludes z-axis data)
		Vector2 startPosition = transform.position;
		endPosition = startPosition + new Vector2 (xDirection, yDirection);

		// Temporarily disable own BoxCollider2D to ensure no collision with ray
		boxCollider.enabled = false;

		// Cast ray and check collision on blocking layer
		hit = Physics2D.Linecast (startPosition, endPosition, blockingLayer);
		boxCollider.enabled = true;

		// Nothing hit, move into new position
		if (hit.transform == null)
		{
            // Check if the intended move collides with an object on the Safe layer and return it
            // TODO: Replace single safe layer with method to check multiple layers if necessary
            hit = Physics2D.Linecast(startPosition, endPosition, GameManager.instance.safeLayer);
            canMove = true;
		}
		else
		{
            canMove = false;
		}

        boxCollider.enabled = true;

        // If object has failed to move, set flag for preventing continuous movement in this direction
        // This can be reset when the object can move again
        if (canMove && IsContinuousMovementStopped())
            ResetContinuousMovement();
        else if (!canMove)
            stopContinuousMovement = true;

        return canMove;
    }

	// Determine if an object can move and return hit object if not
	protected bool Move(int xDirection, int yDirection, out RaycastHit2D hit)
	{
		Vector2 endPosition;

		// Check to see if object can successfully move in the direction given
		// If 'hit' objects are used later that the player can move over, this code may change
		if (CanMove(xDirection, yDirection, out endPosition, out hit) && !stopContinuousMovement)     //!stopContinuousMovement && 
        {
            // Add current position back to game board grid to allow other objects to move here
            bool checkinSuccess = GameManager.instance.boardManager.CheckInGridPosition(this.transform.position);

            if (GameManager.instance.DebugOn())
            {
                if (checkinSuccess)
                    Debug.Log("! Checked in position:      " + this.transform.position.ToString());
                else
                    Debug.Log("X Failed check in position: " + this.transform.position.ToString());
            }

            // Remove the board grid position so no other objects can move here
            // Check for pickup layer to allow moving onto pickups
            bool checkoutSuccess = false;
            Vector3 endGridPosition = new Vector3(endPosition.x, endPosition.y, 0);
            if (hit.transform == null)
            {
                // Remove the board grid position so no other objects can move here
                checkoutSuccess = GameManager.instance.boardManager.CheckOutGridPosition(endGridPosition);
            }
            else
            {
                // The hit object is not blocking (safe), do not check out the grid position but allow move
                checkoutSuccess = true;
            }

            if (GameManager.instance.DebugOn())
            {
                if (checkoutSuccess)
                    Debug.Log("! Checked out position:       " + endGridPosition.ToString());
                else
                    Debug.Log("X Failed check out position:  " + endGridPosition.ToString());
            }

            // Move is allowed, smoothly move to position (if check out succeeded)
            if (checkoutSuccess)
            {
                StartCoroutine(SmoothMovement(endPosition));
                return true;
            }
		}
        
		return false;
	}

	// Co-routine for smooth movement of units to a position
	protected IEnumerator SmoothMovement (Vector3 endPosition)
	{
		objectMoving = true;

		// Calculate remaining distance to move
		// Using "sqrMagnitude" is computationally cheaper than magnitude
		float sqrRemainingDistance = (transform.position - endPosition).sqrMagnitude;

		// While there is still distance
		// Using "float.Epsilon" for near-zero tolerance
		while (sqrRemainingDistance > float.Epsilon)
		{
			// Move towards end position based on move time
			Vector3 newPosition = Vector3.MoveTowards (
				rigidBody.position, endPosition, inverseMoveTime * Time.deltaTime);
			rigidBody.MovePosition (newPosition);

			sqrRemainingDistance = (transform.position - endPosition).sqrMagnitude;

			// Wait a frame before re-evaluating loop
			yield return null;
		}

        //Debug.Log("MovingObject.SmoothMovement: objectMoving = false");
		objectMoving = false;
	}

	// Inheritable virtual function for attempting to move for an object
	//protected virtual void AttemptMove<T> (int xDirection, int yDirection, out RaycastHit2D hit)
	protected virtual bool AttemptMove<T> (int xDirection, int yDirection)
		where T : Component
	{
		if (xDirection == 0 && yDirection == 0)
			return false;

		RaycastHit2D hit;

		// Begin moving the object
		bool moved = Move (xDirection, yDirection, out hit);

		// If no object was hit, return
		if (hit.transform == null)
		{
            // Potential spot for OnCanMove functionality?
            // check for player location here from grid to resolve chance to get stuck in enemy?

			return true;
		}

		// Get a reference to the hit component
		T hitComponent = hit.transform.GetComponent<T> ();

		// If the hit object is something that can be interacted with, call OnCantMove
		if (!moved && hitComponent != null)
			OnCantMove (hitComponent);

		return false;
	}

	// Abstract method with generic parameter to be inherited/implemented when object cannot move
	protected abstract void OnCantMove<T> (T Component)
		where T : Component;

	public bool IsObjectMoving()
	{
		return objectMoving;
	}

    public void ResetContinuousMovement()
    {
        stopContinuousMovement = false;
    }

    public bool IsContinuousMovementStopped()
    {
        return stopContinuousMovement;
    }
}
