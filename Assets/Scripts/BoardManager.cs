﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {

	[Serializable]
	public class Count
	{
		public int maximum;
		public int minimum;

		public Count (int min, int max)
		{
			minimum = min;
			maximum = max;
		}
	}

	static private int PIXELS_PER_GRID_UNIT = 100;	// 100x100 pixels in grid units
	static private int PIXEL_COUNT_TILE = 70;		// 70x70 pixel tiles

	public int columns = 8;							// 8x8 game board
	public int rows = 8;
	public Count wallCount = new Count (3, 6);		// 3-6 inner walls
	public Count oxygenCount = new Count (1, 4);	// 1-4 oxygen tanks
	public GameObject exit;
	public GameObject[] floorTiles;
	public GameObject[] wallTiles;
	public GameObject[] outerWallTiles;
	public GameObject[] oxygenTiles;
	public GameObject[] enemyTiles;

	private Transform boardHolder;		// parent for organization (hierarchy collapsible)
	private List<Vector3> gridPositions = new List<Vector3>();	// board position tracking

	//Vector3 tileScale = new Vector3(1f, 1f, 1f);

	// Initializes collection of game board grid positions
	void InitializeList()
	{
		gridPositions.Clear ();

        //for (int x = 1; x < columns - 1; x++)
        for (int x = 0; x < columns; x++)
        {
            //for (int y = 1; y < rows; y++)
            for (int y = 0; y < rows; y++)
            {
				gridPositions.Add (new Vector3 (x, y, 0f));
			}
		}

        gridPositions.Remove(new Vector3(0, 0, 0));                     // player start position
        gridPositions.Remove(new Vector3(columns - 1, rows - 1, 0f));   // exit tile position
	}

	// Defines inner and outer wall tiles on game board
	void BoardSetup()
	{
		// Create new "Board" GameObject to parent created tiles
		boardHolder = new GameObject ("Board").transform;

		// Loop through columns and rows to create tiles, starting from outside bounds for outer walls
		for (int x = -1; x < columns + 1; x++)
		{
			for (int y = -1; y < rows + 1; y++)
			{
				// Prepare a random inner or outer wall tile prefab to be instantiated, depending on the position
				GameObject tileToCreate;
				bool isOuterTile = false;
				if (x == -1 || x == columns || y == -1 || y == rows)
				{
					tileToCreate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
					isOuterTile = true;
				}
				else
				{
					tileToCreate = floorTiles[Random.Range(0, floorTiles.Length)];
				}

				// Instantiate an instance of the selected tile
				GameObject tileInstance = Instantiate (tileToCreate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;

				FixTileScaling(tileInstance);

				if (isOuterTile)
					tileInstance.layer = LayerMask.NameToLayer("BlockingLayer");

				tileInstance.transform.SetParent (boardHolder);
			}
		}
	}

	// Returns a random position from the game board grid
	public Vector3 RandomPosition()
	{
		// Get a random position from the current available grid positions
		int randomIndex = Random.Range (0, gridPositions.Count);
		Vector3 randomPosition = gridPositions [randomIndex];

		// Remove the selected position so it cannot be selected again
		gridPositions.RemoveAt (randomIndex);

		return randomPosition;
	}

	// Instantiate objects from array between given range at random positions
	void LayoutObjectsAtRandom(GameObject[] tileArray, int minimum, int maximum, int layerMask)
	{
		int objectCount = Random.Range (minimum, maximum + 1);

		for (int i = 0; i < objectCount; i++)
		{
			// Get random unused grid position
			Vector3 randomPosition = RandomPosition ();

			// Get random tile from provided array
			GameObject randomTile = tileArray [Random.Range (0, tileArray.Length)];

			// Instantiate selected tile at designated position
			GameObject randomTileInstance = Instantiate (randomTile, randomPosition, Quaternion.identity);

			// Set specified layer and re-scale instantiated tile
			if (layerMask != -1)
				randomTileInstance.layer = layerMask;

			if (randomTileInstance.tag == "Board" || randomTileInstance.tag == "Exit")
				FixTileScaling(randomTileInstance);
		}
	}

	// Upscales 70x70 tiles to 100x100 board spaces
	void FixTileScaling(GameObject tileToScale)
	{
		float tileScaleValue = (float)PIXELS_PER_GRID_UNIT / (float)PIXEL_COUNT_TILE;
		tileToScale.transform.localScale = new Vector3 (tileScaleValue, tileScaleValue, 1f);
	}

	// Set up game scene by creating board, placing tiles, and defining enemies based on level
	public void SetupScene(int level)
	{
		// Set up board and grid positions
		BoardSetup ();
		InitializeList ();

		LayoutObjectsAtRandom (wallTiles, wallCount.minimum, wallCount.maximum, LayerMask.NameToLayer("BlockingLayer"));
		LayoutObjectsAtRandom (oxygenTiles, oxygenCount.minimum, oxygenCount.maximum, -1);

        // Create enemies based on level (count and type)
        GameManager.instance.enemyManager.GenerateEnemies(level);

		// Create the "exit" tile in the top-right corner
		Instantiate (exit, new Vector3 (columns - 1, rows - 1, 0f), Quaternion.identity);
	}

    /// <summary>
    /// Removes the input position from the game board grid positions.
    /// </summary>
    /// <param name="positionToCheckOut">Grid position to check out</param>
    /// <returns>True if position removed</returns>
    public bool CheckOutGridPosition(Vector3 positionToCheckOut)
    {
        //int checkoutIndex = -1;
        bool checkoutSuccess = false;

        if (gridPositions.Contains(positionToCheckOut))
        {
            //checkoutIndex = gridPositions.IndexOf(positionToCheckOut);
            //gridPositions.RemoveAt(checkoutIndex);
            checkoutSuccess = gridPositions.Remove(positionToCheckOut);
        }

        //return checkoutIndex;
        return checkoutSuccess;
    }

    /// <summary>
    /// Adds the input position to the game board grid positions if not already present.
    /// </summary>
    /// <param name="positionToCheckIn">Grid position to check in</param>
    /// <returns>True if position added</returns>
    public bool CheckInGridPosition(Vector3 positionToCheckIn)
    {
        bool checkinSuccess = false;

        if (!gridPositions.Contains(positionToCheckIn))
        {
            gridPositions.Add(positionToCheckIn);
            checkinSuccess = true;
        }

        return checkinSuccess;
    }

    public int GridCount()
    {
        return gridPositions.Count;
    }

    public Vector3 GridPositionAtIndex(int index)
    {
        return gridPositions[index];
    }
}
