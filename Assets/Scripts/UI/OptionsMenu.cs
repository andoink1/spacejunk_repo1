﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour {

    public Slider volumeSlider;
    public Image volumeOnImage;
    public Toggle volumeToggle;

    private float previousVolume;

    void Start ()
    {
        volumeSlider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        previousVolume = 1.0f;
	}
	
	// Invoked when value of slider changes
    public void ValueChangeCheck()
    {
        if (volumeSlider.value <= 0)
        {
            volumeToggle.isOn = false;
        }
        else
        {
            previousVolume = volumeSlider.value;
            volumeToggle.isOn = true;
        }
    }

    public void AdjustVolumeAfterToggle(bool volumeEnabled)
    {
        if (!volumeEnabled)
        {
            previousVolume = volumeSlider.value;
            volumeSlider.value = 0f;
        }
        else
        {
            if (previousVolume == 0f)
            {
                volumeSlider.value = previousVolume = 1f;
            }
            else
            {
                volumeSlider.value = previousVolume;
            }
        }
    }
}
