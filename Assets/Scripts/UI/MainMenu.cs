﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public GameObject volumeIcon;
    public Sprite volumeOnSprite;
    public Sprite volumeOffSprite;

    private Image volumeImage;
    private bool volumeOn;

    private void Start()
    {
        if (volumeIcon)
        {
            volumeImage = volumeIcon.GetComponent<Image>();
        }
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void ToggleVolume(bool turnOn)
    {
        volumeOn = turnOn;

        if (volumeImage)
        {
            if (volumeOn)
                volumeImage.sprite = volumeOnSprite;
            else
                volumeImage.sprite = volumeOffSprite;
        }
    }
}
