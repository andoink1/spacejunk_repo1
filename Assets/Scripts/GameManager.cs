﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;
	public BoardManager boardManager;
    public EnemyManager enemyManager;
    public PlayerManager playerManager;
    public Utils gameUtils;
	public float levelTitleDelay = 2f;
	public float levelEndDelay = 1f;
	//public float turnDelay = 0.01f;
	public int pointsPerOxygenTank = 10;
	public AudioClip gameOverSound;
	public Text oxygenText;
    public Text levelText;
    public Text pickupText;
    public LayerMask safeLayer;     // Not comparing correctly? Still used in MovingObject.CanMove
    public int level = 1;

    [HideInInspector]
	public bool playersTurn = true;

	private GameObject levelImage;
	//private int level = 1;
	private bool doingSetup;

    private bool debugOn = false;

	void Awake()
	{
        // Set an instance of GameManager if not yet created
        if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);
        
        playerManager = GetComponent<PlayerManager>();
        enemyManager = GetComponent<EnemyManager>();
		boardManager = GetComponent<BoardManager>();
        gameUtils = GetComponent<Utils>();
	}

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Loaded scene: " + scene.name + ", " + mode.ToString());
        InitGame();
    }

	void InitGame()
	{
		doingSetup = true;
        playersTurn = false;

        playerManager.SetupPlayer();
        InitializeUIForNewLevel();
		boardManager.SetupScene (level);
	}

	void Update()
	{
        DebugToggle();

		if (playersTurn || playerManager.IsPlayerMoving() || enemyManager.AreEnemiesMoving() || doingSetup) //player.IsObjectMoving()
            return;

        // Check if the game is over (out of oxygen)
        CheckGameOver();

        if (!enemyManager.AreEnemiesMoving())
        {
            enemyManager.UpdateEnemies();
            playersTurn = true;
        }

        //enemyManager.UpdateEnemies();
	}

	// Check if the player is out of oxygen and end the game
	public void CheckGameOver()
	{
		if (playerManager.IsPlayerOutOfOxygen())
		{
			// Needed here (restart game)? Game ending in this condition
			//initialPlayerOxygen = player.Oxygen;

			AudioManager.instance.PlaySingle(gameOverSound);
			AudioManager.instance.musicSource.Stop();
			enabled = false;
		}
	}

	// Update UI oxygen text by adding input amount (+/-)
	public void UpdateOxygenUI(int amount)
	{
        // Update pickup text for oxygen
        pickupText.text = "";
        if (amount > 1)
            pickupText.text = "+" + amount;
        else if (amount < -1)
            pickupText.text = "-" + amount;

        if (string.IsNullOrEmpty(pickupText.text))
        {
            if (pickupText.enabled)
                pickupText.enabled = false;
        }
        else
        {
            pickupText.rectTransform.position = 
                Camera.main.WorldToScreenPoint(playerManager.CurrentPlayerPosition());
            
            if (!pickupText.enabled)
                pickupText.enabled = true;
        }

        //oxygenText.text = "";
        //if (amount > 1)
        //	oxygenText.text = "+" + amount + " ";
        //else if (amount < -1)
        //	oxygenText.text = "-" + amount + " ";
        //oxygenText.text += "OXYGEN: " + playerManager.CurrentPlayerOxygen();
        oxygenText.text = "OXYGEN: " + playerManager.CurrentPlayerOxygen();
    }

	// Initialize UI components for a new level
	private void InitializeUIForNewLevel()
	{
		oxygenText = GameObject.Find("OxygenText").GetComponent<Text>();
		oxygenText.text = "OXYGEN: " + playerManager.CurrentPlayerOxygen();		// initialPlayerOxygen or player.Oxygen?

        pickupText = GameObject.Find("PickupText").GetComponent<Text>();
        pickupText.text = "";
        pickupText.enabled = false;

		levelImage = GameObject.Find("LevelImage");
		levelImage.SetActive(true);

		levelText = GameObject.Find("LevelText").GetComponent<Text>();
		levelText.text = "AREA " + level;

		Invoke("HideLevelImage", levelTitleDelay);
	}

	private void HideLevelImage()
	{
		levelImage.SetActive(false);
		doingSetup = false;
        playersTurn = true;
    }

    public void LoadNextLevel()
    {
        level++;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void DebugToggle()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            debugOn = !debugOn;
            Debug.Log("DEBUG = " + debugOn.ToString());

            //Debug.Log("====== DEBUG ======");
            //Debug.Log("GridPositions.Count = " + boardManager.GridCount());
            //for (int i = 0; i < 5; i++)
            //    Debug.Log("GridPositions[" + i + "] = " + boardManager.GridPositionAtIndex(i));
            //Debug.Log("===================");
        }
    }

    public bool DebugOn()
    {
        return debugOn;
    }
}
