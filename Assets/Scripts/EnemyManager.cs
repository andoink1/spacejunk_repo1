﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    //public float turnDelay = 0f;      //0.1f, to be removed
    public GameObject[] enemyObjects;

    private List<Enemy> currentEnemies;
    private bool enemiesMoving;

    public List<Enemy> CurrentEnemies
    {
        get { return currentEnemies; }
    }

    public bool AreEnemiesMoving()
    {
        return enemiesMoving;
    }

    // Adds an input Enemy to the game's current enemy list
    public void AddEnemyToList(Enemy enemy)
    {
        currentEnemies.Add(enemy);
    }

    public void GenerateEnemies(int level)
    {
        if (currentEnemies == null)
            currentEnemies = new List<Enemy>();
        else
            currentEnemies.Clear();
        
        // Logarithmic progression based on level
        // L2 = 1 Enemy, L4 = 2 Enemies, L8 = 3 Enemies, etc
        int enemyCount = (int)Mathf.Log(level, 2f);

        // Create the enemies at random available board positions
        // TODO: Add support for multiple enemy types
        InstantiateEnemies(enemyCount);
    }

    public void UpdateEnemies()
    {
        StartCoroutine(MoveEnemies());
    }

    // Move all current enemies with turn delay
    IEnumerator MoveEnemies()
    {
        if (currentEnemies.Count == 0)
        {
            yield break;
        }

        enemiesMoving = true;
        //yield return new WaitForSeconds(turnDelay);

        float longestMoveTime = 0f;

        for (int i = 0; i < currentEnemies.Count; i++)
        {
            currentEnemies[i].MoveEnemy();

            if (currentEnemies[i].moveTime > longestMoveTime)
                longestMoveTime = currentEnemies[i].moveTime;
        }

        yield return new WaitForSeconds(longestMoveTime);

        enemiesMoving = false;
    }

    void InstantiateEnemies(int enemyCount)
    {
        for (int i = 0; i < enemyCount; i++)
        {
            // Get random unused grid position
            Vector3 randomPosition = GameManager.instance.boardManager.RandomPosition();

            // Instantiate random enemy tile at designated position
            GameObject randomEnemyTile = enemyObjects[Random.Range(0, enemyObjects.Length)];
            Instantiate(randomEnemyTile, randomPosition, Quaternion.identity);
        }
    }
}
