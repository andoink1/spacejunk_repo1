﻿using System.Collections;
using UnityEngine;

public class Wall : MonoBehaviour {

	//public Sprite damagedWallSprite;
	public int wallHP = 4;
	//public AudioClip wallHitSound1;

	private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Awake () 
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}

	// Damage the wall by the input value
	public void DamageWall (int damage) 
	{
        // Change the sprite to represent taking damage
        //spriteRenderer.sprite = damagedWallSprite;

        //SoundManager.instance.RandomizeSfx(wallHitSound1, wallHitSound2);

        // Decrement HP and set inactive if HP is 0
        wallHP -= damage;
        if (wallHP <= 0)
        {
            GameManager.instance.boardManager.CheckInGridPosition(this.transform.position);
            gameObject.SetActive(false);
        }
    }
}
