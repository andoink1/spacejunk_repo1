﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject {

	public int playerDamage;
	public AudioClip[] enemyAttacks;

	private Animator animator;
	private Transform target;
	private bool skipMove;	// used for moving enemy every other turn (replace with timer?)

	// Use this for initialization
	protected override void Start ()
	{
        GameManager.instance.enemyManager.AddEnemyToList(this);

		animator = GetComponent<Animator>();
		target = GameObject.FindGameObjectWithTag("Player").transform;
		base.Start();
	}

	protected override bool AttemptMove<T>(int xDirection, int yDirection)
	{
		bool enemyMoved = false;

		enemyMoved = base.AttemptMove<T>(xDirection, yDirection);

        // Skip next enemy move (alternate turns)
		skipMove = true;
        
        // If movement prevented, reset flag to halt continuous movement
        base.ResetContinuousMovement();

		return enemyMoved;
	}

	public void MoveEnemy()
	{
        if (skipMove)
        {
            skipMove = false;
            return;
        }

        int xDirection = 0;
		int yDirection = 0;

		// Determine if X move is needed first, then Y move
		if (!(Mathf.Abs(target.position.x - transform.position.x) < float.Epsilon))
			xDirection = target.position.x > transform.position.x ? 1 : -1;
		else if (!(Mathf.Abs(target.position.y - transform.position.y) < float.Epsilon))
			yDirection = target.position.y > transform.position.y ? 1 : -1;
		else
			return;
        
		// Test that the Enemy can move in this direction	
		RaycastHit2D moveHit;
		Vector2 endPosition;
        bool canEnemyMove = false;

        // Returns true if colliding with Safe layer (oxygen pickup)
        canEnemyMove = base.CanMove(xDirection, yDirection, out endPosition, out moveHit);

        if ((!canEnemyMove && moveHit.transform != null && !GameManager.instance.gameUtils.DoesObjectSupportComponent<Player>(moveHit.transform.gameObject))
            || (canEnemyMove && moveHit.transform != null && GameManager.instance.gameUtils.IsObjectOnSafeLayer(moveHit.transform.gameObject)))
        {
			// If X move attempted, do not move X again,
			// If X not yet attempted (Y attempted), move X towards Exit as strategy
			xDirection = (xDirection != 0) ? 0 : 1;		// replace 1 with direction to Exit?
			yDirection = 0;
           
			// If not moving X (already attempted)
			if (xDirection == 0)
			{
				// If Y position is different than player, move Y towards player
				// Else, move Y towards Exit as strategy
				if (Mathf.Abs(target.position.y - transform.position.y) < float.Epsilon)
					yDirection = 1;						// replace 1 with direction to Exit??
				else
					yDirection = target.position.y > transform.position.y ? 1 : -1;
			}
        }

		AttemptMove<Player>(xDirection, yDirection);
	}

	protected override void OnCantMove<T>(T component)
	{
		Player hitPlayer = component as Player;

		animator.SetTrigger("enemyAttack");

		hitPlayer.LoseHealth(playerDamage);

		bool randomizeAttackSoundPitch = true;
		AudioManager.instance.RandomizeSoundEfx(randomizeAttackSoundPitch, enemyAttacks);
	}
}
