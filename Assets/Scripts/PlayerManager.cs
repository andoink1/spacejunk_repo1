﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public GameObject playerObject;
    public int initialPlayerOxygen = 100;

    private Player player;
    private int previousOxygen = -1;

    private void CreatePlayer()
    {
        //GameObject createdPlayer = Instantiate(playerObject);
        //player = createdPlayer.GetComponent<Player>();

        player = Instantiate(playerObject).GetComponent<Player>();
    }

    private void Update()
    {
        // Set previous oxygen if not already set and player is disabled (changing levels)
        if (previousOxygen != player.Oxygen && player != null && !player.enabled)
            previousOxygen = player.Oxygen;
    }

    public void SetupPlayer()
    {
        CreatePlayer();

        // Use initial oxygen if no previous oxygen yet saved
        if (previousOxygen < 0)
            previousOxygen = initialPlayerOxygen;

        player.Oxygen = previousOxygen;
    }

    public int CurrentPlayerOxygen()
    {
        return player.Oxygen;
    }

    public bool IsPlayerMoving()
    {
        return player.IsObjectMoving();
    }

    public bool IsPlayerOutOfOxygen()
    {
        if (player.Oxygen <= 0)
            return true;
        else
            return false;
    }

    public Vector3 CurrentPlayerPosition()
    {
        return player.transform.position;
    }
}
