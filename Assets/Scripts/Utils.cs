﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour {

	public bool DoesObjectSupportComponent<T>(GameObject gameObject)
    {
        if (gameObject == null)
            return false;

        bool doesObjectHaveComponent = false;

        if (gameObject.GetComponent<T>() != null)
            doesObjectHaveComponent = true;

        return doesObjectHaveComponent;
    }

    public bool IsObjectOnSafeLayer(GameObject gameObject)
    {
        if (gameObject == null)
            return false;

        if (LayerMask.Equals(LayerMask.NameToLayer("Safe"), gameObject.layer))
            return true;

        return false;
    }
}
