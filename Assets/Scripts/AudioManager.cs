﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

	public static AudioManager instance = null;

	public AudioSource efxSource;
	public AudioSource musicSource;
	public float lowPitchRange = 0.95f;
	public float highPitchRange = 1.05f;

	// Use this for initialization
	void Awake () 
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);
		
		DontDestroyOnLoad(gameObject);
	}
	
	// Play single provided audio clip
	public void PlaySingle (AudioClip clip) 
	{
		efxSource.clip = clip;
		efxSource.Play();
	}

	// Play single randomized clip from provided array at random pitch
	public void RandomizeSoundEfx(bool randomizePitch, params AudioClip[] clips)
	{
		int randomIndex = Random.Range(0, clips.Length);
		if (randomizePitch)
		{
			float randomPitch = Random.Range(lowPitchRange, highPitchRange);
			efxSource.pitch = randomPitch;
		}

		efxSource.clip = clips[randomIndex];
		efxSource.Play();
	}
}
